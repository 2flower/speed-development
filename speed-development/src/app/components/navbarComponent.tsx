"use client"
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap'

const NavbarComponent = () => {
  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Container>
        <Navbar.Brand href="/">Speed Development</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/next-templates">NextJS Templates</Nav.Link>
            <NavDropdown title="Pages that don't Exist" id="basic-nav-dropdown">
              <NavDropdown.Item href="#">Captain America</NavDropdown.Item>
              <NavDropdown.Item href="#">Hulk</NavDropdown.Item>
              <NavDropdown.Item href="#">Iron Man</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#">Black Widow</NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default NavbarComponent;