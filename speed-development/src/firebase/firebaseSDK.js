// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCTtLI9C61OjD5Kps0diBsNy5_khCszVLU",
  authDomain: "isaac-ae09e.firebaseapp.com",
  projectId: "isaac-ae09e",
  storageBucket: "isaac-ae09e.appspot.com",
  messagingSenderId: "770830789316",
  appId: "1:770830789316:web:977c03223a67415d4c7791",
  measurementId: "G-MQ30W7MMN4"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);